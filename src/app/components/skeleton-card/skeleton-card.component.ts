import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'skeleton-card',
  templateUrl: './skeleton-card.component.html',
  styleUrls: ['./skeleton-card.component.scss']
})
export class SkeletonCardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
